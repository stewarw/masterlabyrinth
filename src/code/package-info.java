/**
 * Provides the classes needed to make the data structures and game logic
 * for the board game MasterLabyrinth necessary as of Stage 2.
 */
/**
 * @author Michael Langaman
 * @author Tyler Barrett
 * @author Daniel Palacio
 * @author William Stewart
 * @version S.2
 * @since S.1
 */
package code;